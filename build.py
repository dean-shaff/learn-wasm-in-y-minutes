wasm_file_path = "learn-wasm.wast"
template_file_path = "wasm.html.template"
output_file_path = "wasm.html.markdown"

with open(template_file_path, "r") as f:
    template_text = f.read()

with open(wasm_file_path, "r") as f:
    wasm_text = f.read()

with open(output_file_path, "w") as f:
    f.write(
        template_text.format(wasm_text)
    )
